package main // import "vercel-vango.vercel.app/vango-example-gitlab"

import (
	"fmt"

	"vercel-vango.vercel.app/vango-example-gitlab/helloworld"
)

var (
	// Target is exported for testing.
	Target string
)

// Print HelloWorld() results.
func main() {
	fmt.Println(helloworld.Message(Target))
}

// Set the target to "vango".
func init() {
	Target = "vango"
}
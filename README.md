# vango-example-gitlab

`Hello, World!` example for [vercel-vango](https://vercel-vango.vercel.app/).

Get example:

```bash
go get vercel-vango.vercel.app/vango-example-gitlab
```

Testing:

```bash
go test -b vercel-vango.vercel.app/vango-example-gitlab
```

Build binary:

```bash
go build vercel-vango.vercel.app/vango-example-gitlab
```

Install binary:

```bash
go install vercel-vango.vercel.app/vango-example-gitlab
```

Run the installed binary:

```bash
vango-example-gitlab
```
